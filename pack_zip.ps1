param(
	[String]$suffix=""
)
New-Item -Name "output/SKSE/Plugins" -ItemType "directory"
cp -Force target/release/main_menu_randomizer.dll output/SKSE/Plugins
cp -Force MainMenuRandomizer.config.toml output/SKSE/Plugins

Compress-Archive -Path output/SKSE -Force -DestinationPath MainMenuRandomizer${suffix}.zip

